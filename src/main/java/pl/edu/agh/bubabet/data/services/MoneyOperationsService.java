package pl.edu.agh.bubabet.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.bubabet.data.entities.BetSlip;
import pl.edu.agh.bubabet.data.repositories.UserRepository;

@Service
public class MoneyOperationsService {

    private final UserRepository userRepository;

    @Autowired
    public MoneyOperationsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void collectBetSlipMoneyOperation(BetSlip betSlip) {
        //TODO: implement some stuff...
    }
}
