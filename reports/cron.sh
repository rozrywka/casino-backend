#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:
SHELL=/bin/bash

function download_report()
{
    mkdir --parents /ftp/$1/
    curl -H "Authorization: Bearer `cat /tmp/token.key`" http://bubabet-backend:8080/reports/$1/download.csv --output "/ftp/$1/`date '+%Y.%m.%d.%H%M%S'.csv`"
}

download_report payin
download_report payout
download_report stakes_location
download_report stakes_user
download_report refunds
download_report revenue