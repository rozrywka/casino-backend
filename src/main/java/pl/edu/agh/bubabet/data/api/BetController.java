package pl.edu.agh.bubabet.data.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.bubabet.data.entities.Bet;
import pl.edu.agh.bubabet.data.entities.helpers.Result;
import pl.edu.agh.bubabet.data.logs.CreateBetEvent;
import pl.edu.agh.bubabet.data.logs.DeleteBetEvent;
import pl.edu.agh.bubabet.data.logs.ResolveBetEvent;
import pl.edu.agh.bubabet.data.repositories.BetRepository;
import pl.edu.agh.bubabet.data.services.LogService;

import java.util.List;

/**
 * Te endpointy będą służyły to uaktualniania bazy o nowe bety oraz ich update w kwestii stawek
 */

@Controller
@RequestMapping("/bet")
public class BetController {

    private static final String KAFKA_TOPIC = "bet-topic";

    private final BetRepository betRepository;
    private final LogService logService;

    @Autowired
    public BetController(BetRepository betRepository, LogService logService) {
        this.betRepository = betRepository;
        this.logService = logService;
    }


    @PostMapping("/bet")
    public ResponseEntity<Bet> addBet(@RequestBody Bet bet) {
        betRepository.save(bet);
        logService.sendLog(KAFKA_TOPIC, CreateBetEvent.builder().bet(bet).build());
        return new ResponseEntity<>(bet, HttpStatus.OK);
    }

    @PostMapping("/bets")
    public ResponseEntity<List<Bet>> addBets(@RequestBody List<Bet> bets) {
        betRepository.saveAll(bets);
        bets.forEach(bet -> logService.sendLog(KAFKA_TOPIC, CreateBetEvent.builder().bet(bet).build()));
        return new ResponseEntity<>(bets, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<Bet>> getBets() {
        return new ResponseEntity<>(betRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{name}")
    public ResponseEntity<Bet> getBet(@PathVariable String name) {
        try {
            return new ResponseEntity<>(betRepository.findBetByName(name), HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{name}/result")
    public ResponseEntity<Bet> setBetResult(@PathVariable String name, @RequestParam Result result) {
        try {
            Bet bet = betRepository.findBetByName(name);
            bet.setMatchResult(result);
            logService.sendLog(KAFKA_TOPIC,
                ResolveBetEvent.builder().betId(bet.getApiBetId()).result(result).build());
            return new ResponseEntity<>(bet, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{name}")
    public ResponseEntity deleteBet(@PathVariable String name) {
        try {
            Bet betToDel = betRepository.findBetByName(name);
            betRepository.delete(betToDel);
            logService.sendLog(KAFKA_TOPIC, DeleteBetEvent.builder().betId(name).build());
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
