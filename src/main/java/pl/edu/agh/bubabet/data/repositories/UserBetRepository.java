package pl.edu.agh.bubabet.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.bubabet.data.entities.Bet;
import pl.edu.agh.bubabet.data.entities.UserBet;
import pl.edu.agh.bubabet.data.entities.helpers.Result;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface UserBetRepository extends MongoRepository<UserBet, String> {

    List<UserBet> findByBet(Bet bet);

    List<UserBet> findByBettingOdd(BigDecimal bettingOdd);

    List<UserBet> findByBettingOddBetween(BigDecimal from, BigDecimal to);

    List<UserBet> findByBettingOddGreaterThan(BigDecimal bettingOdd);

    List<UserBet> findByBettingOddLessThan(BigDecimal bettingOdd);

    List<UserBet> findByAmount(BigDecimal amount);

    List<UserBet> findByAmountBetween(BigDecimal from, BigDecimal to);

    List<UserBet> findByAmountGreaterThan(BigDecimal amount);

    List<UserBet> findByAmountLessThan(BigDecimal amount);

    List<UserBet> findByAssumedResult(Result assumedResult);
}
