package pl.edu.agh.bubabet.data.repositories.impl.helpers;


import lombok.AllArgsConstructor;
import lombok.Data;
import pl.edu.agh.bubabet.data.entities.CashDesk;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class AmountByCashDesk {
    private CashDesk cashDesk;
    private BigDecimal amount;

    public String getName() {
        return cashDesk.getName();
    }

    public String getAddress() {
        return cashDesk.getAddress();
    }
}
