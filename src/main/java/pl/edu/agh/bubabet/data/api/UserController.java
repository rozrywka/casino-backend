package pl.edu.agh.bubabet.data.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.bubabet.data.entities.MoneyRequestBody;
import pl.edu.agh.bubabet.data.entities.User;
import pl.edu.agh.bubabet.data.logs.LogType;
import pl.edu.agh.bubabet.data.logs.UserEvent;
import pl.edu.agh.bubabet.data.repositories.UserRepository;
import pl.edu.agh.bubabet.data.services.LogService;

import java.math.BigDecimal;
import java.util.List;


@Controller
@RequestMapping("/user")
public class UserController {

    private static final String KAFKA_TOPIC = "user-topic";

    private final UserRepository userRepository;
    private final LogService logService;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserRepository userRepository, LogService logService, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.logService = logService;
        this.passwordEncoder = passwordEncoder;
    }


    @PostMapping("/sign-up")
    public ResponseEntity<User> registerUser(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userRepository.findAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<User> getUserDetailsByName(@PathVariable String name) {
        User user = userRepository.findUserByName(name);
        if (user != null) {
            logService.sendLog(KAFKA_TOPIC, UserEvent.builder().logType(LogType.USER_LOG_IN).userId(name).build());
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        return new ResponseEntity<>(new User(), HttpStatus.NOT_FOUND);
    }


    @GetMapping("/login/{login}")
    public ResponseEntity<User> getUserDetailsByLogin(@PathVariable String login) {
        User user = userRepository.findUserByLogin(login);
        if (user != null) {
            logService.sendLog(KAFKA_TOPIC, UserEvent.builder().logType(LogType.USER_LOG_IN).userId(login).build());
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        return new ResponseEntity<>(new User(), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/login/{login}/addMoney")
    public ResponseEntity<BigDecimal> addMoney(@PathVariable String login, @RequestBody MoneyRequestBody money) {
        List<User> userList = userRepository.findByLogin(login);
        if (userList.size() == 0) {
            return new ResponseEntity<>(BigDecimal.ZERO, HttpStatus.NOT_FOUND);
        }
        User user = userList.get(0);
        user.accountBalance.add(money.amount);
        return new ResponseEntity<>(user.accountBalance, HttpStatus.OK);
    }

    @PostMapping("login//{login}/withdrawMoney")
    public ResponseEntity<BigDecimal> withdrawMoney(@PathVariable String login, @RequestBody MoneyRequestBody money) {
        List<User> userList = userRepository.findByLogin(login);
        if (userList.size() == 0) {
            return new ResponseEntity<>(BigDecimal.ZERO, HttpStatus.NOT_FOUND);
        }
        User user = userList.get(0);
        if (user.accountBalance.subtract(money.amount).compareTo(BigDecimal.ZERO) >= 0) {
            user.accountBalance.subtract(money.amount);
            return new ResponseEntity<>(user.accountBalance, HttpStatus.OK);
        }
        return new ResponseEntity<>(user.accountBalance, HttpStatus.NOT_ACCEPTABLE);
    }
}
