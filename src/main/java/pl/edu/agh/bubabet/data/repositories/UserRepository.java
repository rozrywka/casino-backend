package pl.edu.agh.bubabet.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.bubabet.data.entities.User;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    List<User> findByName(String name);

    User findUserByName(String name);

    User findUserByLogin(String login);

    List<User> findByLogin(String login);

    List<User> findByEmail(String email);
}
