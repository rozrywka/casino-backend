package pl.edu.agh.bubabet.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.edu.agh.bubabet.providers.FeedProvider;

import java.time.LocalDate;

@Controller
public class GamesController {

    @Autowired
    private FeedProvider feedProvider; // TODO replace with ApiFootballFeedProvider after refreshing API Key

    @GetMapping("/leagues")
    public ResponseEntity<String> getLeagues() {
        return new ResponseEntity<>(feedProvider.getLeagues(), HttpStatus.OK);
    }

    @GetMapping("/events/{from}/{to}")
    public ResponseEntity<String> getEvents(@PathVariable String from, @PathVariable String to) {
        return new ResponseEntity<>(feedProvider.getEvents(LocalDate.parse(from), LocalDate.parse(to)), HttpStatus.OK);
    }

    @GetMapping("/odds/{from}/{to}")
    public ResponseEntity<String> getOdds(@PathVariable String from, @PathVariable String to) {
        return new ResponseEntity<>(feedProvider.getOdds(LocalDate.parse(from), LocalDate.parse(to)), HttpStatus.OK);
    }

    @GetMapping("/standings/{leagueId}")
    public ResponseEntity<String> getStandings(@PathVariable int leagueId) {
        return new ResponseEntity<>(feedProvider.getStandings(leagueId), HttpStatus.OK);
    }

    @GetMapping("/head2head/{firstTeam}/{secondTeam}")
    public ResponseEntity<String> getHead2Head(@PathVariable String firstTeam, @PathVariable String secondTeam) {
        return new ResponseEntity<>(feedProvider.getHead2Head(firstTeam, secondTeam), HttpStatus.OK);
    }
}
