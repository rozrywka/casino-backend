package pl.edu.agh.bubabet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.agh.bubabet.utils.SampleDataLoader;

@SpringBootApplication
@RestController
@EnableCaching
public class Application {

    @Autowired
    private SampleDataLoader sampleDataLoader;

    @RequestMapping("/kafka/list/{topic}")
    public String printMessages(@PathVariable String topic){
        return new ExampleKafkaConsumer().listMessages(topic);
    }

    @RequestMapping("/")
    public String home() throws Exception {
        sampleDataLoader.run();
        return "Hello Docker World";
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
