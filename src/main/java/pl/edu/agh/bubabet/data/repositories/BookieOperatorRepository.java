package pl.edu.agh.bubabet.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.bubabet.data.entities.BookieOperator;

import java.util.List;

@Repository
public interface BookieOperatorRepository extends MongoRepository<BookieOperator, String> {

    List<BookieOperator> findByName(String name);

    List<BookieOperator> findByLogin(String login);

    List<BookieOperator> findByEmail(String email);
}
