FROM openjdk:8-jdk-alpine
VOLUME /tmp

ARG JAR_FILE=build/libs/bubabet-backend-0.1.0.jar

COPY ${JAR_FILE} bubabet-backend.jar

EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","bubabet-backend.jar"]
