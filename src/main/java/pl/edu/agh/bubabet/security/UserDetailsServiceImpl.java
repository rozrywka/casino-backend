package pl.edu.agh.bubabet.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pl.edu.agh.bubabet.data.entities.User;
import pl.edu.agh.bubabet.data.repositories.UserRepository;

import java.util.Collections;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<User> loadedUsers = userRepository.findByName(username);
        if (CollectionUtils.isEmpty(loadedUsers)) {
            throw new UsernameNotFoundException(username);
        }
        User user = loadedUsers.get(0);
        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(),
                Collections.emptyList());
    }
}