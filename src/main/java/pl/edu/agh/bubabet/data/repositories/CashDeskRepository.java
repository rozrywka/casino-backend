package pl.edu.agh.bubabet.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.bubabet.data.entities.CashDesk;

import java.util.List;

@Repository
public interface CashDeskRepository extends MongoRepository<CashDesk, String> {

    List<CashDesk> findByName(String name);

    List<CashDesk> findByAddress(String address);

}
