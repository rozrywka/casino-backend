package pl.edu.agh.bubabet.data.entities;

import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookieOperator extends User {

    private List<CashDesk> cashDeskList = new ArrayList<>();

    @Builder
    public BookieOperator(String userId, String name, String login, String email, String password, List<CashDesk> cashDeskList,
                          BigDecimal accountBalance, List<Payment> paymentList, List<BetSlip> betHistory) {
        super(userId, name, login, email, password, accountBalance, paymentList, betHistory);
        this.cashDeskList = cashDeskList;
    }

    public static class BookieOperatorBuilder extends UserBuilder {
        BookieOperatorBuilder() {
            super();
        }
    }
}