package pl.edu.agh.bubabet.providers;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Component
public class ApiFootballFeedProvider implements FeedProvider {

    private static final String HTTPS = "https";
    private static final String API_FOOTBALL = "apifootball.com/api/";
    private static final String API_KEY = "6bc23150c5f7c84223d6cd54952bfeb99a422d13a084f40f4e66c9d3d82c77ff";

    @Override
    @Cacheable(value = "redis", key = "#root.methodName")
    public String getLeagues() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(buildRequestURL("get_leagues").toString(), String.class);
    }

    @Override
    @Cacheable(value = "redis", key = "#root.methodName + #leagueId")
    public String getStandings(int leagueId) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(buildRequestURL("get_standings")
                .addParameter("league_id", Integer.toString(leagueId)).toString(), String.class);
    }

    @Override
    @Cacheable(value = "redis", key = "#root.methodName + #from + #to")
    public String getOdds(LocalDate from, LocalDate to) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(buildRequestURL("get_odds")
                .addParameter("from", from.toString())
                .addParameter("to", to.toString())
                .toString(), String.class);
    }

    @Override
    @Cacheable(value = "redis", key = "#root.methodName + #from + #to")
    public String getEvents(LocalDate from, LocalDate to) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(buildRequestURL("get_events")
                .addParameter("from", from.toString())
                .addParameter("to", to.toString())
                .toString(), String.class);
    }

    @Override
    @Cacheable(value = "redis", key = "#firstTeam + #secondTeam")
    public String getHead2Head(String firstTeam, String secondTeam) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(buildRequestURL("get_H2H")
                .addParameter("firstTeam", firstTeam)
                .addParameter("secondTeam", secondTeam)
                .toString(), String.class);
    }

    private URIBuilder buildRequestURL(String action) {
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme(HTTPS);
        uriBuilder.setHost(API_FOOTBALL);
        uriBuilder.addParameter("action", action);
        uriBuilder.addParameter("APIkey", API_KEY);
        return uriBuilder;
    }
}
