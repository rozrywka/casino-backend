package pl.edu.agh.bubabet.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.bubabet.data.entities.BetSlip;
import pl.edu.agh.bubabet.data.entities.BookieOperator;
import pl.edu.agh.bubabet.data.entities.CashDesk;
import pl.edu.agh.bubabet.data.entities.User;
import pl.edu.agh.bubabet.data.repositories.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import static java.util.stream.Collectors.toList;

@Component
@Slf4j
public class SampleDataLoader {

    private final MongoTemplate mongoTemplate;
    private final BetSlipRepository betSlipRepository;
    private final UserBetRepository userBetRepository;
    private final BetRepository betRepository;
    private final UserRepository userRepository;
    private final BookieOperatorRepository bookieOperatorRepository;
    private final CashDeskRepository cashDeskRepository;
    private final ObjectMapper mapper;

    @Autowired
    public SampleDataLoader(MongoTemplate mongoTemplate, BetSlipRepository betSlipRepository, UserBetRepository userBetRepository,
                            BetRepository betRepository, UserRepository userRepository, BookieOperatorRepository bookieOperatorRepository, CashDeskRepository cashDeskRepository) {
        this.mongoTemplate = mongoTemplate;
        this.betSlipRepository = betSlipRepository;
        this.userBetRepository = userBetRepository;
        this.betRepository = betRepository;
        this.userRepository = userRepository;
        this.bookieOperatorRepository = bookieOperatorRepository;
        this.cashDeskRepository = cashDeskRepository;
        this.mapper = new ObjectMapper();
    }

    @Transactional
    public void run() throws Exception {
        mongoTemplate.getDb().drop();

        try {
            insertData(5);
            log.info("Dropped tables and mocked new data");
        } catch (Exception e) {
            log.error("Cannot insert test data! {}", e.getMessage());
        }
        
    }

    private void insertData(int dataFilesToInsert) throws IOException {

        for (int i = 1; i <= dataFilesToInsert; i++) {
            InputStream dataStream = null;
            try {
                String jsonPath = "/db/test-data/" + "sample-" + i + ".json";
                dataStream = TypeReference.class.getResourceAsStream(jsonPath);
            } catch (NullPointerException e) {
                // there is less files. Just break and let us know about it
                log.warn("There is less data files than expected. Expected: {}, found: {}", dataFilesToInsert, i - 1);
                break;
            }

            Scanner s = new Scanner(dataStream).useDelimiter("\\A");
            String usersJSON = s.hasNext() ? s.next() : "";

            try {
                User user = mapper.readValue(usersJSON, User.class);
                save(user);
                userRepository.save(user);
            } catch (Exception e) {
                throw new RuntimeException("Cannot insert data from file. Current index: " + i + "; " + e.getMessage());
            }
        }
        for (int i = 6; i <= 7; i++) {
            try {
                String jsonPath = "/db/test-data/" + "sample-" + i + ".json";
                InputStream dataStream = TypeReference.class.getResourceAsStream(jsonPath);
                Scanner s = new Scanner(dataStream).useDelimiter("\\A");
                String usersJSON = s.hasNext() ? s.next() : "";
                try {
                    BookieOperator operator = mapper.readValue(usersJSON, BookieOperator.class);
                    List<CashDesk> list = operator.getBetHistory().
                            stream().map(BetSlip::getPurchasedLocation)
                            .collect(toList())
                            .stream()
                            .map(CashDesk.class::cast)
                            .collect(toList());
                    operator.setCashDeskList(list);
                    operator.getCashDeskList().forEach(cashDeskRepository::save);
                    save(operator);
                    bookieOperatorRepository.save(operator);
                } catch (Exception e) {
                    throw new RuntimeException("Cannot insert data from file. Current index: " + i + "; " + e.getMessage());
                }
            } catch (Exception ignored) {
            }
        }
    }


    private void save(User user) {
        // persist test BetSlip
        user.getBetHistory().forEach(betSlipRepository::save);
        // persist test UserBet
        user.getBetHistory().forEach(betSlip -> betSlip.getBetList().forEach(userBetRepository::save));
        // persist test Bet
        // avoid dups by indicating @Id annotation on apiBetId column
        user.getBetHistory().forEach(betSlip -> betSlip.getBetList().forEach(userBet -> betRepository.save(userBet.getBet())));
    }
}
