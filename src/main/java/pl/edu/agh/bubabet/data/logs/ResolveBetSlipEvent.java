package pl.edu.agh.bubabet.data.logs;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResolveBetSlipEvent {

    private final LogType logType = LogType.RESOLVE_BET_SLIP;
    private String betSlipId;
}
