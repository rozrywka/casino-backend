package pl.edu.agh.bubabet.data.entities.helpers;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import pl.edu.agh.bubabet.data.entities.CashDesk;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "typeName")
@JsonSubTypes({@JsonSubTypes.Type(value = OnlineLocation.class, name = "onlineLocation"),
        @JsonSubTypes.Type(value = CashDesk.class, name = "cashDesk")})
public interface Location {
}
