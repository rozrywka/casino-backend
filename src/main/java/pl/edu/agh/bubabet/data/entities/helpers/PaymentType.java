package pl.edu.agh.bubabet.data.entities.helpers;

import lombok.Getter;

@Getter
public enum PaymentType {
    DEPOSIT("DEPOSIT"),
    WITHDRAW("WITHDRAW");

    private String paymentType;

    PaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
