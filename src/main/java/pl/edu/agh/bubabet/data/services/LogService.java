package pl.edu.agh.bubabet.data.services;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class LogService {

    private Map<String, Object> configProps;

    public LogService() {
        configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka1:9092");
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
    }

    public <T> void sendLog(String kafkaTopic, T objectToSend) {
        KafkaTemplate<String, T> kafkaTemplate =
                new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(configProps));
        kafkaTemplate.send(kafkaTopic, objectToSend);
    }

}
