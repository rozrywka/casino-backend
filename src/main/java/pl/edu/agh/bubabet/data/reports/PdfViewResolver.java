package pl.edu.agh.bubabet.data.reports;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.Locale;


public class PdfViewResolver implements ViewResolver {

    @Override
    public View resolveViewName(String s, Locale locale) {
        return new PdfView();
    }
}