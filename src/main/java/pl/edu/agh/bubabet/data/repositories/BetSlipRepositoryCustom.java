package pl.edu.agh.bubabet.data.repositories;

import org.springframework.stereotype.Repository;
import pl.edu.agh.bubabet.data.entities.BookieOperator;
import pl.edu.agh.bubabet.data.entities.helpers.Status;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByUser;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByUser;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface BetSlipRepositoryCustom {

    List<AmountByCashDesk> getPayIn(LocalDate firstDate, LocalDate secondDate, BookieOperator operator);

    List<AmountByCashDesk> getPayOut(LocalDate firstDate, LocalDate secondDate, BookieOperator operator);

    List<AmountByUser> getUsersRevenue(LocalDate firstDate, LocalDate secondDate);

    List<StakesCountByCashDesk> getCashDeskBetSlipsStakesCount(LocalDate firstDate, LocalDate secondDate, Status status, BookieOperator operator);

    List<StakesCountByUser> getUserBetSlipsStakesCount(LocalDate firstDate, LocalDate secondDate);

}
