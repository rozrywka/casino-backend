package pl.edu.agh.bubabet.providers;

import java.time.LocalDate;

public interface FeedProvider {


    String getLeagues();

    String getStandings(int leagueId);

    String getOdds(LocalDate from, LocalDate to);

    String getEvents(LocalDate from, LocalDate to);

    String getHead2Head(String firstTeam, String secondTeam);

}
