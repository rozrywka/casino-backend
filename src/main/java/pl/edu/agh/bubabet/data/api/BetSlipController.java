package pl.edu.agh.bubabet.data.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.bubabet.data.entities.BetSlip;
import pl.edu.agh.bubabet.data.entities.User;
import pl.edu.agh.bubabet.data.entities.helpers.Status;
import pl.edu.agh.bubabet.data.logs.CreateBetSlipEvent;
import pl.edu.agh.bubabet.data.logs.DeleteBetSlipEvent;
import pl.edu.agh.bubabet.data.logs.ResolveBetSlipEvent;
import pl.edu.agh.bubabet.data.repositories.BetSlipRepository;
import pl.edu.agh.bubabet.data.repositories.UserRepository;
import pl.edu.agh.bubabet.data.services.LogService;
import pl.edu.agh.bubabet.data.services.MoneyOperationsService;

import java.util.List;

/**
 * Używając tutejszych endpointów będzie się składało zakłady,
 * potem je zbierało (jeśli będzie co zbierać),
 * będzie też mozna je podejrzeć itp.
 */

@Controller
@RequestMapping("/betslip")
public class BetSlipController {

    private static final String KAFKA_TOPIC = "bet-slip-topic";

    private final LogService logService;
    private final BetSlipRepository betSlipRepository;
    private final UserRepository userRepository;
    private final MoneyOperationsService moneyOperationsService;

    @Autowired
    public BetSlipController(LogService logService, BetSlipRepository betSlipRepository,
                             UserRepository userRepository, MoneyOperationsService moneyOperationsService) {
        this.logService = logService;
        this.betSlipRepository = betSlipRepository;
        this.userRepository = userRepository;
        this.moneyOperationsService = moneyOperationsService;
    }

    @PostMapping
    public ResponseEntity<BetSlip> placeBetSlip(@RequestBody BetSlip betSlip) {

        //TODO: check if there is enough money

        betSlipRepository.save(betSlip);
        logService.sendLog(KAFKA_TOPIC,
                CreateBetSlipEvent.builder().betSlip(betSlip).build());
        return new ResponseEntity<>(betSlip, HttpStatus.OK);
    }


    @GetMapping()
    public ResponseEntity<List<BetSlip>> getAllBetSlips() {
        List<BetSlip> betSlips = betSlipRepository.findAll();
        return new ResponseEntity<>(betSlips, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BetSlip> getBetSlip(@PathVariable String id) {
        try {
            BetSlip betSlip = betSlipRepository.findBetSlipById(id);
            return new ResponseEntity<>(betSlip, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/collect/{id}")
    public ResponseEntity<BetSlip> collectBetSlip(@PathVariable String id) {
        try{
            BetSlip betSlipToCollect = betSlipRepository.findBetSlipById(id);

            moneyOperationsService.collectBetSlipMoneyOperation(betSlipToCollect);

            betSlipToCollect.setBetSlipStatus(Status.COLLECTED);
            logService.sendLog(KAFKA_TOPIC,
                ResolveBetSlipEvent.builder().betSlipId(id).build());
            return new ResponseEntity<>(betSlipToCollect, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteBetSlip(@PathVariable String id) {
        try {
            BetSlip betSlipToDel = betSlipRepository.findBetSlipById(id);
            logService.sendLog(KAFKA_TOPIC,
                    DeleteBetSlipEvent.builder().betSlipId(id).build());
            betSlipRepository.delete(betSlipToDel);

            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/user/{name}")
    public ResponseEntity<List<BetSlip>> getUserBetSlips(@PathVariable String name) {
        try{
            User user = userRepository.findUserByName(name);
            List<BetSlip> userBetSlips = betSlipRepository.findByUserId(user.getUserId());
            return new ResponseEntity<>(userBetSlips, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
