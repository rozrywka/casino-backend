package pl.edu.agh.bubabet.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.bubabet.data.entities.BetSlip;
import pl.edu.agh.bubabet.data.entities.User;
import pl.edu.agh.bubabet.data.entities.helpers.Status;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BetSlipRepository extends MongoRepository<BetSlip, String>, BetSlipRepositoryCustom {

//    BetSlip findBetSlipByBetSlipId(String betSlipId);
    BetSlip findBetSlipById(String id);

    List<BetSlip> findByUserId(String userId);

    List<BetSlip> findByPurchasedDate(LocalDateTime purchasedDate);

    List<BetSlip> findByPurchasedDateBetween(LocalDateTime from, LocalDateTime to);

    List<BetSlip> findByPurchasedDateGreaterThan(LocalDateTime purchasedDate);

    List<BetSlip> findByPurchasedDateLessThan(LocalDateTime purchasedDate);

    List<BetSlip> findByCollectedDate(LocalDateTime collectedDate);

    List<BetSlip> findByCollectedDateBetween(LocalDateTime from, LocalDateTime to);

    List<BetSlip> findByCollectedDateGreaterThan(LocalDateTime collectedDate);

    List<BetSlip> findByCollectedDateLessThan(LocalDateTime collectedDate);

    List<BetSlip> findByBetSlipStatus(Status betSlipStatus);
}
