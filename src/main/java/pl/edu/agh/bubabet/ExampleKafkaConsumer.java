package pl.edu.agh.bubabet;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

class ExampleKafkaConsumer {

    private KafkaConsumer<String, String> consumer;

    ExampleKafkaConsumer() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "kafka1:9092");
        props.put("group.id", "test");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<>(props);
    }

    String listMessages(String topic){
        TopicPartition tp = new TopicPartition(topic, 0);
        List<TopicPartition> tps = Collections.singletonList(tp);
        consumer.assign(tps);
        consumer.seekToBeginning(tps);
        ConsumerRecords<String, String> records = consumer.poll(1000);
        StringBuilder stringBuilder = new StringBuilder();
        for (ConsumerRecord<String, String> record : records)
            stringBuilder.append(String.format(
                    "offset = %d, key = %s, value = %s%n\n", record.offset(), record.key(), record.value()));
        return stringBuilder.toString();
    }


}
