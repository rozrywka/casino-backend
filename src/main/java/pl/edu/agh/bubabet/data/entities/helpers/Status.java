package pl.edu.agh.bubabet.data.entities.helpers;

import lombok.Getter;

@Getter
public enum Status {
    OPEN("OPEN"),
    WON("WON"),
    LOST("LOST"),
    COLLECTED("COLLECTED"),
    CANCELED("CANCELED");

    private String statusName;

    Status(String statusName) {
        this.statusName = statusName;
    }
}
