package pl.edu.agh.bubabet.data.repositories.impl.helpers;


import lombok.AllArgsConstructor;
import lombok.Data;
import pl.edu.agh.bubabet.data.entities.User;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class StakesCountByUser {
    private User user;
    private BigDecimal amount;
    private long count;

    public String getName() {
        return user.getName();
    }

    public String getAddress() {
        return user.getEmail();
    }
}
