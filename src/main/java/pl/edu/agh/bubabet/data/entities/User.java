package pl.edu.agh.bubabet.data.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {

    @Id
    private String userId;
    private String name;
    private String login;
    private String email;
    private String password;
    public BigDecimal accountBalance;
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Payment> paymentList = new ArrayList<>();
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BetSlip> betHistory = new ArrayList<>();
}