package pl.edu.agh.bubabet.data.entities.helpers;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonTypeName("onlineLocation")
public class OnlineLocation implements Location {
    private String ip;
}
