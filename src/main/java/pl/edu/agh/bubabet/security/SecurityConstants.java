package pl.edu.agh.bubabet.security;

public class SecurityConstants {

    public static final String SECRET = "kmkp-23za-dmko-pdo4-mkfi-for9";
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/user/sign-up";
}