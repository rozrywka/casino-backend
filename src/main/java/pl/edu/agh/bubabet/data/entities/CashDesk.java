package pl.edu.agh.bubabet.data.entities;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import pl.edu.agh.bubabet.data.entities.helpers.Location;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonTypeName("cashDesk")
public class CashDesk implements Location {

    @Id
    private String id;
    private String name;
    private String address;
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BookieOperator> operatorList = new ArrayList<>();
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BetSlip> betSlipList = new ArrayList<>();
}