package pl.edu.agh.bubabet.data.reports;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.AbstractView;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByUser;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;


public class PdfView extends AbstractView {

    public PdfView() {
        setContentType("application/pdf");
    }

    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }

    @Override
    protected final void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ByteArrayOutputStream baos = createTemporaryOutputStream();

        Document document = new Document(PageSize.A4.rotate(), 36, 36, 54, 36);
        PdfWriter writer = PdfWriter.getInstance(document, baos);
        prepareWriter(model, writer, request);
        buildPdfMetadata(model, document, request);

        document.open();
        buildPdfDocument(model, document, writer, request, response);
        document.close();

        writeToResponse(response, baos);
    }

    protected void prepareWriter(Map<String, Object> model, PdfWriter writer, HttpServletRequest request) {
        writer.setViewerPreferences(getViewerPreferences());
    }

    protected int getViewerPreferences() {
        return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
    }

    protected void buildPdfMetadata(Map<String, Object> model, Document document, HttpServletRequest request) {
    }

    private void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\"report.pdf\"");

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(100.0f);
        table.setSpacingBefore(10);

        // define font for table header row
        Font font = FontFactory.getFont(FontFactory.TIMES);
        font.setColor(BaseColor.WHITE);

        // define table header cell
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.DARK_GRAY);
        cell.setPadding(5);


        if (model.containsKey("amountByCashDesk")) {
            @SuppressWarnings("unchecked")
            List<AmountByCashDesk> amountByCashDeskList = (List<AmountByCashDesk>) model.get("amountByCashDesk");
            buildAmountByCashDesk(table, cell, font, amountByCashDeskList);
        }
        if (model.containsKey("amountByUser")) {
            @SuppressWarnings("unchecked")
            List<AmountByUser> amountByUser = (List<AmountByUser>) model.get("amountByUser");
            buildAmountByUser(table, cell, font, amountByUser);
        }
        if (model.containsKey("stakesCountByCashDesk")) {
            @SuppressWarnings("unchecked")
            List<StakesCountByCashDesk> stakesCountByCashDeskList = (List<StakesCountByCashDesk>) model.get("stakesCountByCashDesk");
            table.resetColumnCount(4);
            buildStakesCountByCashDesk(table, cell, font, stakesCountByCashDeskList);
        }
        if (model.containsKey("stakesCountByUser")) {
            @SuppressWarnings("unchecked")
            List<StakesCountByUser> stakesCountByUserList = (List<StakesCountByUser>) model.get("stakesCountByUser");
            table.resetColumnCount(4);
            buildStakesCountByUser(table, cell, font, stakesCountByUserList);
        }
        document.add(table);
    }

    private void buildAmountByUser(PdfPTable table, PdfPCell cell, Font font, List<AmountByUser> amountByUserList) {

        // write table header
        cell.setPhrase(new Phrase("User name", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("User email", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Amount", font));
        table.addCell(cell);


        for (AmountByUser amountByUser : amountByUserList) {
            table.addCell(amountByUser.getName());
            table.addCell(amountByUser.getAddress());
            table.addCell(amountByUser.getAmount().toString());
        }
    }

    private void buildStakesCountByCashDesk(PdfPTable table, PdfPCell cell, Font font, List<StakesCountByCashDesk> stakesCountByCashDeskList) {

        // write table header
        cell.setPhrase(new Phrase("Cash desk name", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Cash desk address", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Stakes", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Count", font));
        table.addCell(cell);


        for (StakesCountByCashDesk amountByCashDesk : stakesCountByCashDeskList) {
            table.addCell(amountByCashDesk.getName());
            table.addCell(amountByCashDesk.getAddress());
            table.addCell(amountByCashDesk.getAmount().toString());
            table.addCell(String.valueOf(amountByCashDesk.getCount()));
        }

    }

    private void buildStakesCountByUser(PdfPTable table, PdfPCell cell, Font font, List<StakesCountByUser> stakesCountByUserList) {

        // write table header
        cell.setPhrase(new Phrase("User name", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("User email", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Stakes", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Count", font));
        table.addCell(cell);


        for (StakesCountByUser stakesCountByUser : stakesCountByUserList) {
            table.addCell(stakesCountByUser.getName());
            table.addCell(stakesCountByUser.getAddress());
            table.addCell(stakesCountByUser.getAmount().toString());
            table.addCell(String.valueOf(stakesCountByUser.getCount()));
        }

    }

    private void buildAmountByCashDesk(PdfPTable table, PdfPCell cell, Font font, List<AmountByCashDesk> amountByCashDeskList) {

        // write table header
        cell.setPhrase(new Phrase("Cash desk name", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Cash desk address", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Amount", font));
        table.addCell(cell);


        for (AmountByCashDesk amountByCashDesk : amountByCashDeskList) {
            table.addCell(amountByCashDesk.getName());
            table.addCell(amountByCashDesk.getAddress());
            table.addCell(amountByCashDesk.getAmount().toString());
        }
    }
}
