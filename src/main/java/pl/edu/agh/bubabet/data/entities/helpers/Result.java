package pl.edu.agh.bubabet.data.entities.helpers;

import lombok.Getter;

@Getter
public enum Result {
    FIRST_WON("FIRST_WON"),
    SECOND_WON("SECOND_WON"),
    DRAW("DRAW"),
    NOT_FINISHED("NOT_FINISHED");

    private String resultName;

    Result(String resultName) {
        this.resultName = resultName;
    }
}
