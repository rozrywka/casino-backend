package pl.edu.agh.bubabet.data.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.edu.agh.bubabet.data.entities.helpers.MatchCategory;
import pl.edu.agh.bubabet.data.entities.helpers.Result;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Bet {

    private String apiBetId;
    private String name;
    private String firstTeamName;
    private String secondTeamName;
    private String description;
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime finishedDate;
    private MatchCategory matchCategory;
    private Result matchResult;
}
