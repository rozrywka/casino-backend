package pl.edu.agh.bubabet.data.entities.helpers;

import lombok.Getter;

@Getter
public enum MatchCategory {
    ENGLISH_LEAGUE("ENGLISH_LEAGUE"),
    FRENCH_LEAGUE("FRENCH_LEAGUE");

    private String categoryName;

    MatchCategory(String categoryName) {
        this.categoryName = categoryName;
    }
}
