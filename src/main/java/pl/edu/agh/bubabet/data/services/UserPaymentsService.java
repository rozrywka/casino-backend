package pl.edu.agh.bubabet.data.services;

import org.springframework.stereotype.Service;
import pl.edu.agh.bubabet.data.entities.Payment;
import pl.edu.agh.bubabet.data.entities.User;
import pl.edu.agh.bubabet.data.entities.helpers.PaymentType;
import pl.edu.agh.bubabet.data.repositories.UserRepository;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByUser;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserPaymentsService {

    @Resource
    private UserRepository userRepository;

    public List<AmountByUser> getUsersWalletRefunds() {
        List<User> userList = userRepository.findAll();
        List<AmountByUser> amountByUserList = new ArrayList<>();

        for (User user : userList) {
            BigDecimal amount = user.getPaymentList().stream()
                    .filter(e -> e.getPaymentType().equals(PaymentType.WITHDRAW))
                    .map(Payment::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            amountByUserList.add(new AmountByUser(user, amount));
        }
        return amountByUserList;
    }

    public BigDecimal getUserPaymentsSum(User user) {
        BigDecimal depositAmount = user.getPaymentList().stream()
                .filter(e -> e.getPaymentType().equals(PaymentType.DEPOSIT))
                .map(Payment::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal withdrawAmount = user.getPaymentList().stream()
                .filter(e -> e.getPaymentType().equals(PaymentType.WITHDRAW))
                .map(Payment::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return depositAmount.subtract(withdrawAmount);

    }
}
