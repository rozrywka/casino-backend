# BubaBet

## Build
Create JAR:
```
./gradlew build
```

Create docker image:
```
./gradlew docker
```

You can use use docker compose to launch an app. App is available at [localhost:8080](http://localhost:8080):
```
docker-compose up
```

How to register and log in user:
```
localhost:8080/user/sign-up with POST like:
{
"name": "user",
"password": "password"
}

localhost:8080/login with POST like:
{
"name": "user",
"password": "password"
}
```
Second endpoint will return token which you can reuse for calling another endpoints.