package pl.edu.agh.bubabet.data.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.agh.bubabet.data.entities.helpers.Status;
import pl.edu.agh.bubabet.data.repositories.BetSlipRepository;
import pl.edu.agh.bubabet.data.repositories.BookieOperatorRepository;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByUser;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByUser;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;


@Controller
@RequestMapping("/reports")
public class ReportsController {

    @Resource
    private UserPaymentsService userPaymentsService;

    @Resource
    private BetSlipRepository betSlipRepository;

    @Resource
    private BookieOperatorRepository bookieOperatorRepository;

    @GetMapping("/refunds")
    public ResponseEntity<List<AmountByUser>> getUsersRefunds() {
        List<AmountByUser> list = userPaymentsService.getUsersWalletRefunds();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/revenue")
    public ResponseEntity<List<AmountByUser>> getUsersRevenue() {
        List<AmountByUser> list = betSlipRepository.getUsersRevenue(LocalDate.now().minusYears(3), LocalDate.now());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/payin")
    public ResponseEntity<List<AmountByCashDesk>> getPayIn() {
        List<AmountByCashDesk> list = betSlipRepository.getPayIn(LocalDate.now().minusYears(3), LocalDate.now(),
                bookieOperatorRepository.findById("7").get());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/payout")
    public ResponseEntity<List<AmountByCashDesk>> getPayOut() {
        List<AmountByCashDesk> list = betSlipRepository.getPayOut(LocalDate.now().minusYears(3), LocalDate.now(),
                bookieOperatorRepository.findById("7").get());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/stakes_location")
    public ResponseEntity<List<StakesCountByCashDesk>> getBetSlipStakesCountLocation() {
        List<StakesCountByCashDesk> list = betSlipRepository.getCashDeskBetSlipsStakesCount(LocalDate.now().minusYears(3), LocalDate.now(),
                Status.WON, bookieOperatorRepository.findById("7").get());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/stakes_user")
    public ResponseEntity<List<StakesCountByUser>> getBetSlipStakesCountUser() {
        List<StakesCountByUser> list = betSlipRepository.getUserBetSlipsStakesCount(LocalDate.now().minusYears(3), LocalDate.now());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/payin/download")
    public String getPayInDownload(Model model) {
        model.addAttribute("amountByCashDesk", betSlipRepository.getPayIn(LocalDate.now().minusYears(3), LocalDate.now(),
                bookieOperatorRepository.findById("7").get()));
        return "";
    }

    @GetMapping("/payout/download")
    public String getPayOutDownload(Model model) {
        model.addAttribute("amountByCashDesk", betSlipRepository.getPayOut(LocalDate.now().minusYears(3), LocalDate.now(),
                bookieOperatorRepository.findById("7").get()));
        return "";
    }

    @GetMapping("/stakes_location/download")
    public String getBetSlipStakesCountLocationDownload(Model model) {
        model.addAttribute("stakesCountByCashDesk", betSlipRepository.getCashDeskBetSlipsStakesCount(LocalDate.now().minusYears(3), LocalDate.now(),
                Status.WON, bookieOperatorRepository.findById("7").get()));
        return "";
    }

    @GetMapping("/stakes_user/download")
    public String getBetSlipStakesCountUserDownload(Model model) {
        model.addAttribute("stakesCountByUser", betSlipRepository.getUserBetSlipsStakesCount(LocalDate.now().minusYears(3), LocalDate.now()));
        return "";
    }

    @GetMapping("/refunds/download")
    public String getUsersRefundsDownload(Model model) {
        model.addAttribute("amountByUser", userPaymentsService.getUsersWalletRefunds());
        return "";
    }

    @GetMapping("/revenue/download")
    public String getUsersRevenueDownload(Model model) {
        model.addAttribute("amountByUser", betSlipRepository.getUsersRevenue(LocalDate.now().minusYears(3), LocalDate.now()));
        return "";
    }

}
