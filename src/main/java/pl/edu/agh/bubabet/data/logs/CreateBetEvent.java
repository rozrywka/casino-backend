package pl.edu.agh.bubabet.data.logs;

import lombok.*;
import pl.edu.agh.bubabet.data.entities.Bet;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateBetEvent {

    private final LogType logType = LogType.CREATE_BET;
    private Bet bet;
}
