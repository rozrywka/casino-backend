package pl.edu.agh.bubabet.data.logs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.edu.agh.bubabet.data.entities.BetSlip;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateBetSlipEvent {

    private final LogType logType = LogType.RESOLVE_BET;
    private BetSlip betSlip;
    private String userMail;
}
