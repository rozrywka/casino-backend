package pl.edu.agh.bubabet.data.reports;

import lombok.Setter;
import org.springframework.web.servlet.view.AbstractView;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByUser;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Setter
public class CsvView extends AbstractView {

    private static final String CONTENT_TYPE = "text/csv";
    private String url;


    public CsvView() {
        setContentType(CONTENT_TYPE);
    }


    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }


    @Override
    protected final void renderMergedOutputModel(
            Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType(getContentType());
        buildCsvDocument(model, request, response);
    }

    private void buildCsvDocument(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setHeader("Content-Disposition", "attachment; filename=\"report.csv\"");

        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
                CsvPreference.STANDARD_PREFERENCE);

        if (model.containsKey("amountByCashDesk")) {
            @SuppressWarnings("unchecked")
            List<AmountByCashDesk> amountByCashDeskList = (List<AmountByCashDesk>) model.get("amountByCashDesk");
            buildAmountByCashDesk(csvWriter, amountByCashDeskList);
        }
        if (model.containsKey("amountByUser")) {
            @SuppressWarnings("unchecked")
            List<AmountByUser> amountByUser = (List<AmountByUser>) model.get("amountByUser");
            buildAmountByUser(csvWriter, amountByUser);
        }
        if (model.containsKey("stakesCountByCashDesk")) {
            @SuppressWarnings("unchecked")
            List<StakesCountByCashDesk> stakesCountByCashDeskList = (List<StakesCountByCashDesk>) model.get("stakesCountByCashDesk");
            buildStakesCountByCashDesk(csvWriter, stakesCountByCashDeskList);
        }
        if (model.containsKey("stakesCountByUser")) {
            @SuppressWarnings("unchecked")
            List<StakesCountByUser> stakesCountByUserList = (List<StakesCountByUser>) model.get("stakesCountByUser");
            buildStakesCountByUser(csvWriter, stakesCountByUserList);
        }

        csvWriter.close();
    }

    private void buildAmountByUser(ICsvBeanWriter csvWriter, List<AmountByUser> amountByUserList) throws Exception {

        String[] header = {"User name", "User email", "Amount"};
        String[] fields = {"name", "address", "amount"};


        csvWriter.writeHeader(header);

        for (AmountByUser amountByUser : amountByUserList) {
            csvWriter.write(amountByUser, fields);
        }
    }

    private void buildStakesCountByCashDesk(ICsvBeanWriter csvWriter, List<StakesCountByCashDesk> stakesCountByCashDeskList) throws Exception {

        String[] header = {"Cash desk name", "Cash desk address", "Stakes", "Count"};
        String[] fields = {"name", "address", "amount", "count"};


        csvWriter.writeHeader(header);

        for (StakesCountByCashDesk stakesCountByCashDesk : stakesCountByCashDeskList) {
            csvWriter.write(stakesCountByCashDesk, fields);
        }

    }

    private void buildStakesCountByUser(ICsvBeanWriter csvWriter, List<StakesCountByUser> stakesCountByUserList) throws Exception {
        String[] header = {"User name", "User email", "Stakes", "Count"};
        String[] fields = {"name", "address", "amount"};


        csvWriter.writeHeader(header);

        for (StakesCountByUser stakesCountByUser : stakesCountByUserList) {
            csvWriter.write(stakesCountByUser, fields);
        }

    }

    private void buildAmountByCashDesk(ICsvBeanWriter csvWriter, List<AmountByCashDesk> amountByCashDeskList) throws Exception {

        String[] header = {"Cash desk name", "Cash desk address", "Amount"};
        String[] fields = {"name", "address", "amount"};


        csvWriter.writeHeader(header);

        for (AmountByCashDesk amountByCashDesk : amountByCashDeskList) {
            csvWriter.write(amountByCashDesk, fields);
        }
    }

}