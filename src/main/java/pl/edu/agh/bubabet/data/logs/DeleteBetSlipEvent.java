package pl.edu.agh.bubabet.data.logs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeleteBetSlipEvent {

    private final LogType logType = LogType.DELETE_BET_SLIP;
    private String betSlipId;
}
