package pl.edu.agh.bubabet.providers;

import org.apache.commons.io.IOUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Optional;

@Component
@Primary
public class JsonFileFeedProvider implements FeedProvider {

    @Cacheable(value = "redis", key = "#root.methodName")
    @Override
    public String getLeagues() {
        return read("get_leagues.json");
    }

    @Cacheable(value = "redis", key = "#root.methodName + #leagueId")
    @Override
    public String getStandings(int leagueId) {
        return Optional.ofNullable(read("get_standings&league_id=" + leagueId + ".json"))
                .orElse(read("get_standings&league_id=63.json"));
    }

    @Cacheable(value = "redis", key = "#root.methodName + #from + #to")
    @Override
    public String getOdds(LocalDate from, LocalDate to) {
        return read("get_odds&from=2018-12-01&to=2018-12-05.json");
    }

    @Cacheable(value = "redis", key = "#root.methodName + #from + #to")
    @Override
    public String getEvents(LocalDate from, LocalDate to) {
        return read("get_events&from=2018-11-01&to=2019-03-30.json");
    }

    @Cacheable(value = "redis", key = "#root.methodName + #firstTeam + #secondTeam")
    @Override
    public String getHead2Head(String firstTeam, String secondTeam) {
        return Optional.ofNullable(read("get_H2H&firstTeam=" + firstTeam + "&secondTeam=" + secondTeam + ".json"))
                .orElse(read("get_H2H&firstTeam=Reading&secondTeam=Brentford.json"));
    }

    private String read(String filename) {
        ClassPathResource resource = new ClassPathResource("apifootball/" + filename);
        try {
            InputStream inputStream = resource.getInputStream();
            return IOUtils.toString(inputStream, "UTF-8");
        } catch (IOException e) {
            return null;
        }
    }
}
