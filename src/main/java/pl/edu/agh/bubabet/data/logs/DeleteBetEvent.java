package pl.edu.agh.bubabet.data.logs;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeleteBetEvent {

    private final LogType logType = LogType.DELETE_BET;
    private String betId;
}