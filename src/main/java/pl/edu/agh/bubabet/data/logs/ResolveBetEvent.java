package pl.edu.agh.bubabet.data.logs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.edu.agh.bubabet.data.entities.helpers.Result;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResolveBetEvent {

    private final LogType logType = LogType.RESOLVE_BET;
    private String betId;
    private Result result;
}
