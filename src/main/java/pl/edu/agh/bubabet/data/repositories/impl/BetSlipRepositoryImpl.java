package pl.edu.agh.bubabet.data.repositories.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import pl.edu.agh.bubabet.data.entities.*;
import pl.edu.agh.bubabet.data.entities.helpers.OnlineLocation;
import pl.edu.agh.bubabet.data.entities.helpers.Status;
import pl.edu.agh.bubabet.data.repositories.BetSlipRepositoryCustom;
import pl.edu.agh.bubabet.data.repositories.UserRepository;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByUser;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByUser;
import pl.edu.agh.bubabet.data.services.UserPaymentsService;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Slf4j
public class BetSlipRepositoryImpl implements BetSlipRepositoryCustom {

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private UserRepository userRepository;

    @Resource
    private UserPaymentsService userPaymentsService;


    @Override
    public List<AmountByCashDesk> getPayIn(LocalDate firstDate, LocalDate secondDate, BookieOperator operator) {
        List<AggregationOperation> operations = Collections.singletonList(
                getDateMatchOperation(firstDate, secondDate));

        List<BetSlip> betSlipList = filterBetSlip(operations);
        Map<CashDesk, List<BetSlip>> betSlipByLocationMap = getBetSlipListByLocation(betSlipList, operator);

        List<AmountByCashDesk> amountByCashDeskList = new ArrayList<>();
        for (Map.Entry<CashDesk, List<BetSlip>> entry : betSlipByLocationMap.entrySet()) {
            BigDecimal amount = entry.getValue().stream()
                    .map(BetSlip::getBetList)
                    .flatMap(Collection::stream)
                    .map(UserBet::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            amountByCashDeskList.add(new AmountByCashDesk(entry.getKey(), amount));
        }
        return amountByCashDeskList;
    }

    @Override
    public List<StakesCountByCashDesk> getCashDeskBetSlipsStakesCount(LocalDate firstDate, LocalDate secondDate, Status status, BookieOperator operator) {
        List<AggregationOperation> operations = Collections.singletonList(
                getDateMatchOperation(firstDate, secondDate));

        List<BetSlip> betSlipList = filterBetSlip(operations);
        Map<CashDesk, List<BetSlip>> betSlipByLocationMap = getBetSlipListByLocation(betSlipList, operator, status);

        List<StakesCountByCashDesk> stakesCountByCashDeskList = new ArrayList<>();
        for (Map.Entry<CashDesk, List<BetSlip>> entry : betSlipByLocationMap.entrySet()) {
            BigDecimal amount = getStake(entry.getValue());

            long count = entry.getValue().size();

            stakesCountByCashDeskList.add(new StakesCountByCashDesk(entry.getKey(), amount, count));
        }
        return stakesCountByCashDeskList;
    }

    @Override
    public List<AmountByCashDesk> getPayOut(LocalDate firstDate, LocalDate secondDate, BookieOperator operator) {
        List<AggregationOperation> operations = Arrays.asList(
                getDateMatchOperation(firstDate, secondDate),
                getWinnerMatchOperation());
        List<BetSlip> betSlipList = filterBetSlip(operations);
        Map<CashDesk, List<BetSlip>> betSlipByLocationMap = getBetSlipListByLocation(betSlipList, operator);

        List<AmountByCashDesk> amountByCashDeskList = new ArrayList<>();
        for (Map.Entry<CashDesk, List<BetSlip>> entry : betSlipByLocationMap.entrySet()) {
            BigDecimal amount = entry.getValue().stream()
                    .map(BetSlip::getBetList)
                    .flatMap(Collection::stream)
                    .map(bet -> bet.getAmount().multiply(bet.getBettingOdd()))
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            amountByCashDeskList.add(new AmountByCashDesk(entry.getKey(), amount));
        }
        return amountByCashDeskList;
    }

    @Override
    public List<StakesCountByUser> getUserBetSlipsStakesCount(LocalDate firstDate, LocalDate secondDate) {
        Map<User, List<BetSlip>> betSlipByUserMap = getUserBetSlip(firstDate, secondDate);

        List<StakesCountByUser> stakesCountByUserList = new ArrayList<>();
        for (Map.Entry<User, List<BetSlip>> entry : betSlipByUserMap.entrySet()) {

            BigDecimal amount = getStake(entry.getValue());
            long count = entry.getValue().size();

            stakesCountByUserList.add(new StakesCountByUser(entry.getKey(), amount, count));
        }
        return stakesCountByUserList;
    }

    @Override
    public List<AmountByUser> getUsersRevenue(LocalDate firstDate, LocalDate secondDate) {
        Map<User, List<BetSlip>> betSlipByUserMap = getUserBetSlip(firstDate, secondDate);

        List<AmountByUser> amountByUserList = new ArrayList<>();
        for (Map.Entry<User, List<BetSlip>> entry : betSlipByUserMap.entrySet()) {

            BigDecimal paymentsAmount = userPaymentsService.getUserPaymentsSum(entry.getKey());

            BigDecimal winAmount = entry.getValue().stream()
                    .filter(e -> e.getBetSlipStatus().equals(Status.COLLECTED) || e.getBetSlipStatus().equals(Status.WON))
                    .map(BetSlip::getBetList)
                    .flatMap(Collection::stream)
                    .map(bet -> bet.getAmount().multiply(bet.getBettingOdd()))
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            BigDecimal loseAmount = entry.getValue().stream()
                    .map(BetSlip::getBetList)
                    .flatMap(Collection::stream)
                    .map(bet -> bet.getAmount().multiply(bet.getBettingOdd()))
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            amountByUserList.add(new AmountByUser(entry.getKey(), paymentsAmount.add(winAmount).subtract(loseAmount)));
        }
        return amountByUserList;
    }

    private BigDecimal getStake(List<BetSlip> betSlipList) {
        return betSlipList.stream()
                .map(BetSlip::getBetList)
                .flatMap(Collection::stream)
                .map(UserBet::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private Map<User, List<BetSlip>> getUserBetSlip(LocalDate firstDate, LocalDate secondDate) {
        List<AggregationOperation> operations = Collections.singletonList(
                getDateMatchOperation(firstDate, secondDate));

        List<BetSlip> betSlipList = filterBetSlip(operations);
        return getBetSlipListByUser(betSlipList);
    }

    private Map<User, List<BetSlip>> getBetSlipListByUser(List<BetSlip> betSlipList) {
        return betSlipList.stream()
                .filter(e -> e.getPurchasedLocation() instanceof OnlineLocation)
                .collect(Collectors.groupingBy(e -> userRepository.findById(e.getUserId()).get()));
    }

    private Map<CashDesk, List<BetSlip>> getBetSlipListByLocation(List<BetSlip> betSlipList, BookieOperator operator) {
        return betSlipList.stream()
                .filter(e -> e.getPurchasedLocation() instanceof CashDesk)
                .filter(e -> operator.getCashDeskList().contains((CashDesk) e.getPurchasedLocation()))
                .collect(Collectors.groupingBy(e -> (CashDesk) e.getPurchasedLocation()));
    }

    private Map<CashDesk, List<BetSlip>> getBetSlipListByLocation(List<BetSlip> betSlipList, BookieOperator operator, Status status) {
        return betSlipList.stream()
                .filter(e -> e.getBetSlipStatus().equals(status))
                .filter(e -> e.getPurchasedLocation() instanceof CashDesk)
                .filter(e -> operator.getCashDeskList().contains((CashDesk) e.getPurchasedLocation()))
                .collect(Collectors.groupingBy(e -> (CashDesk) e.getPurchasedLocation()));
    }

    private List<BetSlip> filterBetSlip(List<AggregationOperation> operations) {
        Aggregation aggregation = Aggregation.newAggregation(operations);
        return mongoTemplate.aggregate(aggregation, "betSlip", BetSlip.class).getMappedResults();
    }

    private MatchOperation getDateMatchOperation(LocalDate firstDate, LocalDate secondDate) {
        Criteria criteria = where("purchasedDate").lte(secondDate)
                .gte(firstDate);
        return match(criteria);
    }

    private MatchOperation getWinnerMatchOperation() {
        Criteria criteria = where("betSlipStatus").in(Arrays.asList(Status.WON, Status.COLLECTED));
        return match(criteria);
    }
}
