package pl.edu.agh.bubabet.data.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.edu.agh.bubabet.data.entities.helpers.Result;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserBet {

    private Bet bet;
    /**
     * The current ratio number during the bet.
     * Payment when winning = amount * bettingOdd.
     */
    private BigDecimal bettingOdd;
    /**
     * Amount paid by the user.
     */
    private BigDecimal amount;
    /**
     * The expected result of the match by the user.
     */
    private Result assumedResult;
}
