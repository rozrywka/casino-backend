package pl.edu.agh.bubabet.data.reports;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.AmountByUser;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByCashDesk;
import pl.edu.agh.bubabet.data.repositories.impl.helpers.StakesCountByUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ExcelView extends AbstractXlsxView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) {

        response.setHeader("Content-Disposition", "attachment; filename=\"report.xlsx\"");

        Sheet sheet = workbook.createSheet("Report");
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_80_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
        style.setFont(font);

        if (model.containsKey("amountByCashDesk")) {
            @SuppressWarnings("unchecked")
            List<AmountByCashDesk> amountByCashDeskList = (List<AmountByCashDesk>) model.get("amountByCashDesk");
            buildAmountByCashDesk(sheet, style, amountByCashDeskList);
        }
        if (model.containsKey("amountByUser")) {
            @SuppressWarnings("unchecked")
            List<AmountByUser> amountByUser = (List<AmountByUser>) model.get("amountByUser");
            buildAmountByUser(sheet, style, amountByUser);
        }
        if (model.containsKey("stakesCountByCashDesk")) {
            @SuppressWarnings("unchecked")
            List<StakesCountByCashDesk> stakesCountByCashDeskList = (List<StakesCountByCashDesk>) model.get("stakesCountByCashDesk");
            buildStakesCountByCashDesk(sheet, style, stakesCountByCashDeskList);
        }
        if (model.containsKey("stakesCountByUser")) {
            @SuppressWarnings("unchecked")
            List<StakesCountByUser> stakesCountByUserList = (List<StakesCountByUser>) model.get("stakesCountByUser");
            buildStakesCountByUser(sheet, style, stakesCountByUserList);
        }
    }


    private void buildAmountByUser(Sheet sheet, CellStyle style, List<AmountByUser> amountByUserList) {

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("User name");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("User email");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Amount");
        header.getCell(2).setCellStyle(style);

        int rowCount = 1;

        for (AmountByUser amountByUser : amountByUserList) {
            Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(amountByUser.getName());
            userRow.createCell(1).setCellValue(amountByUser.getAddress());
            userRow.createCell(2).setCellValue(amountByUser.getAmount().toString());
        }
    }

    private void buildStakesCountByCashDesk(Sheet sheet, CellStyle style, List<StakesCountByCashDesk> stakesCountByCashDeskList) {

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Cash desk name");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Cash desk address");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Stakes");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("Count");
        header.getCell(3).setCellStyle(style);
        int rowCount = 1;

        for (StakesCountByCashDesk stakesCountByCashDesk : stakesCountByCashDeskList) {
            Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(stakesCountByCashDesk.getName());
            userRow.createCell(1).setCellValue(stakesCountByCashDesk.getAddress());
            userRow.createCell(2).setCellValue(stakesCountByCashDesk.getAmount().toString());
            userRow.createCell(3).setCellValue(String.valueOf(stakesCountByCashDesk.getCount()));

        }

    }

    private void buildStakesCountByUser(Sheet sheet, CellStyle style, List<StakesCountByUser> stakesCountByUserList) {

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("User name");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("User email");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Stakes");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("Count");
        header.getCell(3).setCellStyle(style);

        int rowCount = 1;

        for (StakesCountByUser stakesCountByUser : stakesCountByUserList) {
            Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(stakesCountByUser.getName());
            userRow.createCell(1).setCellValue(stakesCountByUser.getAddress());
            userRow.createCell(2).setCellValue(stakesCountByUser.getAmount().toString());
            userRow.createCell(3).setCellValue(String.valueOf(stakesCountByUser.getCount()));
        }

    }

    private void buildAmountByCashDesk(Sheet sheet, CellStyle style, List<AmountByCashDesk> amountByCashDeskList) {

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Cash desk name");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Cash desk address");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Amount");
        header.getCell(2).setCellStyle(style);

        int rowCount = 1;

        for (AmountByCashDesk amountByCashDesk : amountByCashDeskList) {
            Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(amountByCashDesk.getName());
            userRow.createCell(1).setCellValue(amountByCashDesk.getAddress());
            userRow.createCell(2).setCellValue(amountByCashDesk.getAmount().toString());
        }
    }

}