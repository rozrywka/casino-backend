package pl.edu.agh.bubabet.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.bubabet.data.entities.Bet;
import pl.edu.agh.bubabet.data.entities.helpers.MatchCategory;
import pl.edu.agh.bubabet.data.entities.helpers.Result;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BetRepository extends MongoRepository<Bet, String> {

    Bet findBetByName(String name);

    Bet findBetByApiBetId(String name);

    List<Bet> findByMatchCategory(MatchCategory matchCategory);

    List<Bet> findByFinishedDate(LocalDateTime finishedDate);

    List<Bet> findByFinishedDateBetween(LocalDateTime from, LocalDateTime to);

    List<Bet> findByFinishedDateGreaterThan(LocalDateTime finishedDate);

    List<Bet> findByFinishedDateLessThan(LocalDateTime finishedDate);

    List<Bet> findByMatchResult(Result matchResult);
}
